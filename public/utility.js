function PassData(data) {
    alert(data);
    window.myData = data;
    //return data;
}

function setAccessToken(token) {
    //TrackJS.track("setAccessToken Called");
    var test = getParameterByName('test')
      ? getParameterByName('test').toLowerCase()
      : false;
    var iotSource = getParameterByName('source')
      ? getParameterByName('source')
      : '';
    var iotSourceId = getParameterByName('sourcename')
      ? getParameterByName('sourcename')
      : '';

    window.requestFocus();
    window.getData(token, JSON.parse(test), iotSource, iotSourceId);
  }

  function requestFocus() {
    try {
      JSAndroidBridge.requestFocus();
    } catch (e) {}
  }

  function getAccessToken() {
    //TrackJS.track('getAccessToken Called');
    switch (getMobileOperatingSystem()) {
      case 'android':
        //To work on Android
        JSAndroidBridge.getAccessToken();
        break;
      default:
        //To work on iPhone
        window.location.href = '#getAccessToken';
    }
  }

  function onSoftRefresh() {
    //TrackJS.track('onSoftRefresh Called');
    window.softRefresh();
  }

  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2]);
  }

  function onSoftRefreshWithPayload(payload) {
    //TrackJS.track('onSoftRefresh Payload Called');
    window.softRefresh();
  }